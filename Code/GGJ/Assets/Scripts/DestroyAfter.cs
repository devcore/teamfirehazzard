﻿using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour 
{
    public float duration = 1.5f;
	// Use this for initialization
	
	// Update is called once per frame
	void Update () 
    {
        duration -= Time.deltaTime;
        if (duration < 0.0f)
        {
            Destroy(gameObject);
        }
        //Debug.Log(duration);
	}
}
