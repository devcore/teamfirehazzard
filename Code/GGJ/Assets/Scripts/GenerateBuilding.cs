﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateBuilding : MonoBehaviour 
{
    public List<GameObject> rooms;
    //public Transform prefeb;

	// Use this for initialization
	void Start ()
    {
        if (rooms[0].name == "ROOM001")
        {
            rooms[0].tag = "Kitchen";
            rooms[1].tag = "Living Room";
            rooms[2].tag = "Kitchen";
            rooms[3].tag = "Games Room";
            rooms[4].tag = "Toilet";
            rooms[5].tag = "Bedroom";
            rooms[6].tag = "Bathroom";
            rooms[7].tag = "Bedroom";
            rooms[8].tag = "Bedroom";
            rooms[9].tag = "Toilet";
        }
        else if (rooms[0].name == "ROOM002")
        {
            rooms[0].tag = "Sleep Desk";
            rooms[1].tag = "Vending Machine";
            rooms[2].tag = "Machine";
            rooms[3].tag = "Machine";
            rooms[4].tag = "Machine";
            rooms[5].tag = "Sleep Desk";
            rooms[6].tag = "Toilet";
            rooms[7].tag = "Vending Machine";
            rooms[8].tag = "Sleep Desk";
            rooms[9].tag = "Sleep Desk";
            rooms[10].tag = "Sleep Desk";
            rooms[11].tag = "Toilet";
            rooms[12].tag = "Sleep Desk";
            rooms[13].tag = "Toilet";
        }
        else if (rooms[0].name == "ROOM003")
        {
            rooms[0].tag = "Toilet";
            rooms[1].tag = "Arcade";
            rooms[2].tag = "Cafe";
            rooms[3].tag = "Arcade";
            rooms[4].tag = "Fast Food";
            rooms[5].tag = "Arcade";
            rooms[6].tag = "Cinema";
            rooms[7].tag = "Toilet";
            rooms[8].tag = "Cinema";
            rooms[9].tag = "Cafe";
            rooms[10].tag = "Fast Food";
            rooms[11].tag = "Toilet";
            rooms[12].tag = "Cafe";
            rooms[13].tag = "Fast Food";
            rooms[14].tag = "Arcade";
            rooms[15].tag = "Cinema";
            rooms[16].tag = "Cinema";
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
