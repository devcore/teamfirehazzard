﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour 
{
    public float g_rotationSpeed;
	
	void Update () 
	{
        gameObject.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 0, 1), g_rotationSpeed * Time.deltaTime); 
	}
}
